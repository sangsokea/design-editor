/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_PIXABAY_KEY: "30646743-97cc0ac93f59d135a4e10eeba"
  readonly VITE_APP_PEXELS_KEY: string
}
//30646743-97cc0ac93f59d135a4e10eeba
interface ImportMeta {
  readonly env: ImportMetaEnv
}
